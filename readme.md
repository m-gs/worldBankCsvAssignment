Assignment:
===========

Write a program that extracts the data and emits 3 different CSV files with the following data:
1. List the top 15 countries by GDP, for the year 2005, in descending order with 4 columns showing GDP (constant 2005 US$), GNI (constant 2005 US$), GDP per capita (constant 2005 US$), GNI per capita (constant 2005 US$).  

2. List GDP growth for India over the given time period. Two columns, one is the year and second one is the annual growth from the previous year. 

3. List the aggregated "GDP per capita (constant 2005 US$)" by continent, over the time period 1960-2015. The continents to country mapping you have to create yourself. Use wikipedia to get this list and create a data structure to hold this.

Notes:
1. Data source: http://data.worldbank.org/data-catalog/world-development-indicators
2. Download the CSV version of this data set.
3. Filter the data on the field "Indicator Code" basis the Indicator Name contents.
4. All the "data" is in the file WDI_Data.csv. The rest are all metadata.
5. If data cell is blank, do not take it
