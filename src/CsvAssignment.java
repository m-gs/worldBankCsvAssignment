
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CsvAssignment {
	static String csvFile = "WDI_Data.csv";

	//First Solution
	public static void firstSolution() {
		String csvOpForFirstSolution = "FirstSolution.csv";
		TreeMap<Double, String> countriesByGdp = null;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		String[] details;

		countriesByGdp = new TreeMap<Double, String>(Collections.reverseOrder());
		String key="";
		try 
		{
			br = new BufferedReader(new FileReader(csvFile));
			boolean countryStart = false;
			while ((line = br.readLine()) != null) {
				details = line.split(cvsSplitBy,60);

				//Avoiding data till the country list arrives first by Afghanistan
				if(countryStart == false){

					if("Afghanistan".equals(details[0])){
						countryStart = true;
					}
					continue;
				}
				if("GDP at market prices (constant 2005 US$)".equals(details[2]) && !(key=details[49]).isEmpty()){
					String temp =  (details[0]+","+key);
					do{
						line = br.readLine();
						details = line.split(cvsSplitBy,60);


						if(!(details[49].isEmpty()) && ("GDP per capita (constant 2005 US$)".equals(details[2]) | "GNI (constant 2005 US$)".equals(details[2]) | "GNI per capita (constant 2005 US$)".equals(details[2]))){
							temp+=","+details[49];
						}

					} while(!"GNI per capita (constant 2005 US$)".equals(details[2]));
					countriesByGdp.put(Double.parseDouble(key), temp);
				}


			}

			//Generate Csv File For First Solution
			int count = 0;
			FileWriter writer = new FileWriter(csvOpForFirstSolution);
			for(Map.Entry<Double, String> entry : countriesByGdp.entrySet()){
				if(count==0) {
					writer.append("Country,GDP (constant 2005 US$),GDP per capita (constant 2005 US$),GNI (constant 2005 US$),GNI per capita (constant 2005 US$)\n");
				}
				else if(count==15) break;
				writer.append(entry.getValue());
				writer.append('\n');
				count++;
			}
			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		System.out.println("CSV file for First Solution Generated Successfully, check the 'FirstSolution.csv'!!!");
	}

	//Second Solution
	public static void secondSolution(int firstYear, int lastYear) {
		String csvOpForSecondSolution = "SecondSolution.csv";
		Map<String, Double> gdpGrowth = null;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		String[] details;

		gdpGrowth = new LinkedHashMap<String, Double>();
		try 
		{
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) 
			{
				details = line.split(cvsSplitBy,60);
				if("India".equals(details[0]) && "GDP growth (annual %)".equals(details[2])){
					int yearIndex = 1960;
					//int firstYear = 1968;
					//int lastYear = 2004;
					int firstIndex = firstYear - yearIndex + 4;

					yearIndex = firstYear;
					for(int i=firstIndex; i<60 && yearIndex <=lastYear;i++, yearIndex++){

						if(details[i].isEmpty()){
							gdpGrowth.put(String.valueOf(yearIndex), 0.0E+0);
						}else {
							gdpGrowth.put(String.valueOf(yearIndex), Double.parseDouble(details[i]));
						}
					}
					break;
				}
			}

			//Generate Csv File For Second Solution
			boolean isColumn = true;
			FileWriter writer = new FileWriter(csvOpForSecondSolution);
			for(Map.Entry<String, Double> entry : gdpGrowth.entrySet()){
				if(isColumn==true) {
					writer.append("Year,Growth\n");
					isColumn = false;
				}
				writer.append(entry.getKey()+","+String.valueOf(entry.getValue()));
				writer.append('\n');
			}
			writer.flush();
			writer.close();

		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		System.out.println("CSV file for Second Solution Generated Successfully, check the 'SecondSolution.csv'!!!");
	}


	//Third Solution
	public static void thirdSolution() {
		String csvOpForThirdSolution = "ThirdSolution.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
		String[] details;

		//ASIA
		double asiaGdp = 0.0;
		Set<String> asiaSet = new HashSet<String>();
		asiaSet.add("India");
		asiaSet.add("Bangladesh");
		asiaSet.add("China");
		asiaSet.add("Mongolia");
		asiaSet.add("Korea, Rep.");
		asiaSet.add("Korea, Dem. Rep.");
		asiaSet.add("Vietnam");
		asiaSet.add("Lao PDR");
		asiaSet.add("Myanmar");
		asiaSet.add("Cambodia");
		asiaSet.add("Indonesia");
		asiaSet.add("Philippines");
		asiaSet.add("Malaysia");
		asiaSet.add("Singapore");
		asiaSet.add("Afghanistan");
		asiaSet.add("Bahrain");
		asiaSet.add("Bhutan");
		asiaSet.add("Brunei Darussalam");
		asiaSet.add("Iran, Islamic Rep.");
		asiaSet.add("Iraq");
		asiaSet.add("Israel");
		asiaSet.add("Japan");
		asiaSet.add("Jordan");
		asiaSet.add("Kazakhstan");
		asiaSet.add("Kuwait");
		asiaSet.add("Kyrgyz Republic");
		asiaSet.add("Lebanon");
		asiaSet.add("Maldives");
		asiaSet.add("Nepal");
		asiaSet.add("Oman");
		asiaSet.add("Pakistan");
		asiaSet.add("Qatar");
		asiaSet.add("Saudi Arabia");
		asiaSet.add("Sri Lanka");
		asiaSet.add("Syrian Arab Republic");
		asiaSet.add("Tajikistan");
		asiaSet.add("Thailand");
		asiaSet.add("Turkmenistan");
		asiaSet.add("United Arab Emirates");
		asiaSet.add("Uzbekistan");
		asiaSet.add("Yemen, Rep.");
		asiaSet.add("Macao SAR, China");
		asiaSet.add("Hong Kong SAR, China");

		//EUROPE
		double europeGdp = 0.0;
		Set<String> europeSet = new HashSet<String>();
		europeSet.add("Albania");
		europeSet.add("Armenia");
		europeSet.add("Austria");
		europeSet.add("Azerbaijan");
		europeSet.add("Belarus");
		europeSet.add("Belgium");
		europeSet.add("Bosnia and Herzegovina");
		europeSet.add("Bulgaria");
		europeSet.add("Croatia");
		europeSet.add("Czech Republic");
		europeSet.add("Denmark");
		europeSet.add("Estonia");
		europeSet.add("Finland");
		europeSet.add("France");
		europeSet.add("Georgia");
		europeSet.add("Germany");
		europeSet.add("Greece");
		europeSet.add("Hungary");
		europeSet.add("Iceland");
		europeSet.add("Ireland");
		europeSet.add("Italy");
		europeSet.add("Latvia");
		europeSet.add("Liechtenstein");
		europeSet.add("Lithuania");
		europeSet.add("Luxembourg");
		europeSet.add("Malta");
		europeSet.add("Moldova");
		europeSet.add("Monaco");
		europeSet.add("Montenegro");
		europeSet.add("Netherlands");
		europeSet.add("Norway");
		europeSet.add("Poland");
		europeSet.add("Portugal");
		europeSet.add("Cyprus");
		europeSet.add("Macedonia, FYR");
		europeSet.add("Romania");
		europeSet.add("Russia");
		europeSet.add("Serbia");
		europeSet.add("Slovak Republic");
		europeSet.add("Slovenia");
		europeSet.add("Spain");
		europeSet.add("Sweden");
		europeSet.add("Switzerland");
		europeSet.add("Turkey");
		europeSet.add("Ukraine");
		europeSet.add("United Kingdom");


		//AFRICA
		double africaGdp = 0.0;
		Set<String> africaSet = new HashSet<String>();
		africaSet.add("Algeria");
		africaSet.add("Angola");
		africaSet.add("Benin");
		africaSet.add("Botswana");
		africaSet.add("Burkina Faso");
		africaSet.add("Burundi");
		africaSet.add("Cabo Verde");
		africaSet.add("Cameroon");
		africaSet.add("Central African Republic");
		africaSet.add("Chad");
		africaSet.add("Comoros");
		africaSet.add("Congo, Dem. Rep.");
		africaSet.add("Congo, Rep.");
		africaSet.add("Cote d'Ivoire");
		africaSet.add("Djibouti");
		africaSet.add("Egypt, Arab Rep.");
		africaSet.add("Equatorial Guinea");
		africaSet.add("Eritrea");
		africaSet.add("Ethiopia");
		africaSet.add("Gabon");
		africaSet.add("Gambia");
		africaSet.add("Ghana");
		africaSet.add("Guinea-Bissau");
		africaSet.add("Guinea");
		africaSet.add("Kenya");
		africaSet.add("Lesotho");
		africaSet.add("Liberia");
		africaSet.add("Libya");
		africaSet.add("Madagascar");
		africaSet.add("Malawi");
		africaSet.add("Mali");
		africaSet.add("Mauritania");
		africaSet.add("Mauritius");
		africaSet.add("Morocco");
		africaSet.add("Mozambique");
		africaSet.add("Namibia");
		africaSet.add("Niger");
		africaSet.add("Nigeria");
		africaSet.add("Rwanda");
		africaSet.add("Sao Tome and Principe");
		africaSet.add("Senegal");
		africaSet.add("Seychelles");
		africaSet.add("Sierra Leone");
		africaSet.add("Somalia");
		africaSet.add("South Africa");
		africaSet.add("South Sudan");
		africaSet.add("Sudan");
		africaSet.add("Swaziland");
		africaSet.add("Tanzania");
		africaSet.add("Togo");
		africaSet.add("Tunisia");
		africaSet.add("Uganda");
		africaSet.add("Zambia");
		africaSet.add("Zimbabwe");


		//SOUTH AMERICA
		double southAmericaGdp = 0.0;
		Set<String> southAmericaSet = new HashSet<String>();
		southAmericaSet.add("Argentina");
		southAmericaSet.add("Bolivia");
		southAmericaSet.add("Brazil");
		southAmericaSet.add("Chile");
		southAmericaSet.add("Colombia");
		southAmericaSet.add("Ecuador");
		southAmericaSet.add("Guyana");
		southAmericaSet.add("Paraguay");
		southAmericaSet.add("Peru");
		southAmericaSet.add("Suriname");
		southAmericaSet.add("Uruguay");
		southAmericaSet.add("Venezuela, RB");


		//NORTH AMERICA
		double northAmericaGdp = 0.0;
		Set<String> northAmericaSet = new HashSet<String>();
		northAmericaSet.add("Antigua and Barbuda");
		northAmericaSet.add("Bahamas, The");
		northAmericaSet.add("Barbados");
		northAmericaSet.add("Belize");
		northAmericaSet.add("Canada");
		northAmericaSet.add("Costa Rica");
		northAmericaSet.add("Cuba");
		northAmericaSet.add("Dominica");
		northAmericaSet.add("Dominican Republic");
		northAmericaSet.add("El Salvador");
		northAmericaSet.add("Grenada");
		northAmericaSet.add("Guatemala");
		northAmericaSet.add("Haiti");
		northAmericaSet.add("Honduras");
		northAmericaSet.add("Jamaica");
		northAmericaSet.add("Mexico");
		northAmericaSet.add("Nicaragua");
		northAmericaSet.add("Panama");
		northAmericaSet.add("St. Kitts and Nevis");
		northAmericaSet.add("St. Lucia");
		northAmericaSet.add("St. Vincent and The Grenadines");
		northAmericaSet.add("Trinidad and Tobago");
		northAmericaSet.add("United States");

		//OCEANIA
		double oceaniaGdp = 0.0;
		Set<String> oceaniaSet = new HashSet<String>();
		oceaniaSet.add(" Australia");
		oceaniaSet.add("Papua New Guinea");
		oceaniaSet.add("Fiji");
		oceaniaSet.add("Kiribati");
		oceaniaSet.add("Marshall Islands");
		oceaniaSet.add("Micronesia, Fed. Sts.");
		oceaniaSet.add("New Zealand");
		oceaniaSet.add("Palau");
		oceaniaSet.add("Samoa");
		oceaniaSet.add("Solomon Islands");
		oceaniaSet.add("Tonga");
		oceaniaSet.add("Tuvalu");
		oceaniaSet.add("Vanuatu");
		oceaniaSet.add("American Samoa");
		oceaniaSet.add("French Polynesia");
		oceaniaSet.add("Guam");
		oceaniaSet.add("New Caledonia");
		oceaniaSet.add("Northern Mariana Islands");

		//ANTARCTICA not inhabited
		//double antarcticaGdp = 0.0;
		//Set<String> antarcticaSet = new HashSet<String>();
		//double uncategorizedGdp=0.0;//For checking only

		int blankAsia=0;
		int blankAfrica=0;
		int blankNorthAmerica=0;
		int blankSouthAmerica=0;
		int blankOceania=0;
		int blankEurope=0;


		try 
		{
			br = new BufferedReader(new FileReader(csvFile));
			boolean countryStart = false;
			String countryName;
			while ((line = br.readLine()) != null) {
				details = line.split(cvsSplitBy,60);

				//Avoiding data till the country list arrives first by Afghanistan
				if(countryStart == false){

					if("Afghanistan".equals(countryName = details[0])){
						countryStart = true;
					}
					continue;
				}
				
				
				if("GDP per capita (constant 2005 US$)".equals(details[2])){
					countryName = details[0];

					if(africaSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankAfrica++;
							}
							else africaGdp+=Double.parseDouble(details[i]);
						}
					}
					else if(europeSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankEurope++;
							}
							else europeGdp+=Double.parseDouble(details[i]);
						}
					} else if(asiaSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankAsia++;
							}
							else asiaGdp+=Double.parseDouble(details[i]);
						}
					} else if(northAmericaSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankNorthAmerica++;
							}
							else northAmericaGdp+=Double.parseDouble(details[i]);
						}
					} else if(southAmericaSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankSouthAmerica++;
							}
							else southAmericaGdp+=Double.parseDouble(details[i]);
						}
					} else if(oceaniaSet.contains(countryName)){
						for(int i=4;i<60;i++){
							if(details[i].isEmpty()){
								blankOceania++;
							}
							else oceaniaGdp+=Double.parseDouble(details[i]);
						}
					}

				}
			}

			//Generate Csv File For Third Solution
			FileWriter writer = new FileWriter(csvOpForThirdSolution);
			writer.append("Continent,Aggregate GDP\n");
			writer.append("Asia,"+String.valueOf(asiaGdp/((56*asiaSet.size())-blankAsia))+"\n");
			writer.append("Africa,"+String.valueOf(africaGdp/((56*africaSet.size())-blankAfrica))+"\n");
			writer.append("North America,"+String.valueOf(northAmericaGdp/((56*northAmericaSet.size())-blankNorthAmerica))+"\n");
			writer.append("South America,"+String.valueOf(southAmericaGdp/((56*southAmericaSet.size())-blankSouthAmerica))+"\n");
			writer.append("Europe,"+String.valueOf(europeGdp/((56*europeSet.size())-blankEurope))+"\n");
			writer.append("Oceania,"+String.valueOf(oceaniaGdp/((56*oceaniaSet.size())-blankOceania))+"\n");
			writer.append("Antarctica,NA\n");

			writer.flush();
			writer.close();
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			if (br != null) 
			{
				try 
				{
					br.close();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
		System.out.println("CSV file for Third Solution Generated Successfully, check the 'ThirdSolution.csv'!!!");
	}



	public static void main(String[] args) 
	{
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			System.out.print("###Worldbank Data:\n "
					+ "1. Generate first 15 countries by GDP. \n "
					+ "2. India's GDP Growth. \n "
					+ "3. Aggregate GDP by Continent. \n "
					+ "4. Generate All the Above. \n "
					+ "Enter your option: ");
			String option = br.readLine();

			switch (option) {
			case "1":
				firstSolution();
				break;
			case "2":
				System.out.print("Enter an option:\n"
						+ "1. Generate for all available data from 1960 to 2015.\n"
						+ "2. Choose a period of years to display. \n "
						+ "Enter your option: ");
				String option01 = br.readLine();
				switch(option01){
				case "1":
					secondSolution(1960, 2015);
					break;
				case "2":
					System.out.print("Starting Year: ");
					int firstYear = Integer.parseInt(br.readLine());
					System.out.print("Ending Year: ");
					int lastYear = Integer.parseInt(br.readLine());
					secondSolution(firstYear, lastYear);
					break;
				default:
					System.out.println("Invalid Option!!");
					break;
				}
				break;
			case "3":
				thirdSolution();
				break;
			case "4":
				firstSolution();
				secondSolution(1960, 2015);
				thirdSolution();
			default:
				System.out.println("Invalid Option!!");
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
